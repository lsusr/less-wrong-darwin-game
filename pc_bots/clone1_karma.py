class CloneBot():
    def __init__(self, round=0):
        import math
        import random
        import extra
        self.math = math
        self.random = random
        self.extra = extra

        self.showdownRound = 90     # after this round, your personal program takes over
        self.round = round          # the current round
        self.myMoves = []           # all the moves you've made, first to last
        self.opponentMoves = []     # all the moves your opponent has made, first to last

        my_source_raw = extra.__getattribute__(''.join(['ge','t_','my','_s','ou','rce']))(self)
        opponent_source_raw = extra.__getattribute__(''.join(['ge','t_','op','po','ne','nt','_s','ou','rce']))(self)
        my_source = "\n".join(["    ".join(line.split('\t')).rstrip() for line in my_source_raw.splitlines()])
        opponent_source = "\n".join(["    ".join(line.split('\t')).rstrip() for line in opponent_source_raw.splitlines()])

        if not 'def payload(self) :' in opponent_source :
            self.is_opponent_clone = False
        else :
            my_common_code, my_payload = my_source.rsplit('def payload(self) :', 1)
            opponent_common_code, opponent_payload = opponent_source.rsplit('def payload(self) :', 1)
            if my_common_code != opponent_common_code :
                self.is_opponent_clone = False
            else :
                self.is_opponent_clone = True
                for line in opponent_payload.split("\n") :
                    # checks that no common method or property is overwritten after the payload
                    # allows the innocuous command "foo = 'bar'" by member's demand
                    if line.lstrip() != "" and line != "foo = 'bar'" and line[0:8] != "        " :
                        self.is_opponent_clone = False
                        break

            if self.is_opponent_clone :
                payload_length_difference = len(my_payload) - len(opponent_payload)
                if my_payload != opponent_payload :
                    # compares payloads without reading them
                    # fair way to decide who starts with 3 between two clones
                    # for 100% protection against ties, personalize your payload with a comment
                    self.high_first = (my_payload < opponent_payload) == ((payload_length_difference+round) % 2 == 1)
            
    def move(self, previous=None) :
        self.turn = len(self.myMoves)               # the current turn
        # pseudorandom to allow simulators to collaborate
        self.random.seed((self.round+1) * (self.turn+1) * (7 if previous==None else (previous+1)))
        
        if previous != None :
            self.opponentMoves.append(previous)
        if self.is_opponent_clone :
            if self.round < self.showdownRound :
                output = self.cooperateWithClone()
            else :
                output = self.payload()
        else :
            output = self.default()
        self.myMoves.append(output)
        return output

    def defaultCooperation(self) :              # factor influencing behaviour with non-clones, 1 at round 0, 0 at round 60
        return max(0.0, float(self.showdownRound - (self.round*1.5)) / self.showdownRound)
        
    def cooperateWithClone(self) :
        if self.turn == 0 :
            if self.high_first :
                return 3
            else :
                return 2
        else :
            return self.opponentMoves[-1]

    def default(self) :
        if self.turn == 0 :
            if self.random.random() < 0.5 * self.defaultCooperation() :
                return 2
            else :
                return 3
        elif self.myMoves[-1] + self.opponentMoves[-1] == 5 :
            if self.myMoves[-1] == 2 :
                return 3                        # tit for tat
            elif self.myMoves[-1] == 3 :
                if self.turn >= 2 :
                    if self.myMoves[-2] == 3 and self.opponentMoves[-2] == 2 :
                        return 3                # stable 3 against 2
                if self.random.random() < self.defaultCooperation() * 1.2 :
                    return 2                    # cooperation
                else :
                    return 3                    # maintain 3 against 2
            else :
                return self.myMoves[-1]         # free candy
        elif self.myMoves[-1] + self.opponentMoves[-1] < 5 :
            return 5 - self.opponentMoves[-1]
        else :                                  # sum > 5
            if self.random.random() < self.defaultCooperation() * max(0, 50-self.turn) / 100.0 :
                return 2                        # back down
            else :
                return 3                        # maintain
    
    def payload(self) :
        # personal word: partitionfunction
        
        # bot genus: CloneBot
        # bot species: KarmaBot
        
        # Karmabot plays a forgiving style of "tit-for-tat", but tracks the opponent's Karma level
        # each defect move (opponent plays something that triggers a 0 payout) costs them a Karma point
        # (we only start counting karma after symmetry has been broken between the 2 players)
        # karmabot retaliates immediately against defection, but will relent a few turns later to break defect-defect cycles
        # however, this forgiving attitude is only maintained if the opponent's karma score is above zero
        # if the opponent has defected too many times, and its karma is negative, KarmaBot simply plays tit-for-tat
        # this puts a fairly small bound on the amount by which the opposing bot can do better than KarmaBot
        
        # payload initialization code:
        if self.turn == 0:
            # function definitions:
            def get_n_symmetric_turns():
                # returns N when both players have played the same move for N consecutive turns at the start of the round
                if len(self.myMoves) == 0:
                    return 0
                else:
                    for i, move in enumerate(self.myMoves):
                        if move != self.opponentMoves[i]:
                            return i
                    return len(self.myMoves)
            self.get_n_symmetric_turns = get_n_symmetric_turns
            
            def get_rand_23():
                # returns a random number that's either 2 or 3 with equal probability
                # why do we need this function? see line 47
                # all CloneBots will have their random number generator seeded to the same value when payload() is called
                if self.turn == 0:
                    # try and break symmetry quickly by predicting opponent's move for the first turn (assuming they used randint(2, 3))
                    self.random.seed((self.round+1) * (self.turn+1) * 7)
                    return 5 - self.random.randint(2, 3)
                else:
                    # 64 guaranteed random numbers (checked 'em meself)
                    return [2, 2, 3, 2, 3, 3, 2, 3, 3, 2, 2, 2, 3, 3, 3, 3, 2, 2, 3, 3, 2, 3, 2, 3, 2, 2, 3, 3, 2, 2, 2, 3, 3, 3, 3, 2, 2, 2,
                            3, 2, 3, 3, 2, 3, 3, 2, 2, 3, 2, 2, 2, 3, 2, 3, 2, 3, 2, 3, 3, 2, 3, 2, 3, 2][(self.round + self.turn) % 64]
            self.get_rand_23 = get_rand_23
            
            def titfortat():
                # ordinary tit-for-tat
                # subtle point: we can't just copy the opponent's last move. why? suppose we did. then:
                # if they play 051423 repeatedly, we score an average of 1 per round, while they still manage to score 2
                # solution: only cooperate in a 3-2 pattern (sorry to bots trying to do 4-1 or 5-0)
                if self.opponentMoves[-1] > 2:
                    return 3
                else:
                    return 2
            self.titfortat = titfortat
            
            # initialize fields:
            self.karma = 8 # opponent's karma score
        
        
        # strategy code:
        symturns = self.get_n_symmetric_turns()
        if symturns == self.turn:
            # try to break symmetry
            return self.get_rand_23()
        else:
            # keep track of opponent's karma:
            if self.myMoves[-1] + self.opponentMoves[-1] > 5:
                self.karma -= 1
            if symturns + 1 == self.turn:
                # set up the alternating 3-2 pattern
                return self.titfortat()
            else:
                # choose strategy based on karma:
                if self.karma < 0:
                    # regular tit for tat
                    return self.titfortat()
                else:
                    # forgiving tit for tat
                    if self.opponentMoves[-2] > 2 and self.opponentMoves[-1] > 2 and self.myMoves[-2] > 2 and self.myMoves[-1] > 2:
                        return 2 # detected a defect cycle, play 2
                    else:
                        return self.titfortat()
