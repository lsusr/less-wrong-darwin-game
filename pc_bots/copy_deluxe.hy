(defclass CopyBotDeluxe []
  (defn --init-- [self &optional [round 0]]
    (setv self.turn 0))
  (defn move [self &optional [previous None]]
    (+= self.turn 1)
    (if (= self.turn 1) 3
        (= self.turn 2) 2
        previous)))
