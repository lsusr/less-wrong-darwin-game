(defclass Cooperator []
  (defn --init-- [self &optional [round 1]]
    (setv self.round round))
  (defn move [self &optional [previous None]]
    (lif previous
         previous
         2)))
