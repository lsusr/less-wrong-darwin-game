(import random)
(defclass PureTFT []
  (defn --init-- [self &optional [round 1]]
    (setv self.previous None))
  (defn move [self &optional [previous None]]
    (lif previous
         previous
         (random.randint 2 3))))
