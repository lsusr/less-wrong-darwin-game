class WinnerAgainstLowConstant():
    def __init__(self, round=1):
        self.turn = 0
        self.round = round
        self.previous = None
    def move(self, previous=None):
        self.turn += 1
        if self.previous:
            output = self.previous
            self.previous = previous
            return 5-output
        else:
            return 2
