(import [extra [get-opponent-source]])
;; handshakeid

(defn turn-score [p1 p2]
  (if (> (+ p1 p2) 5)
      0
      p1))

(defclass JacobBot []
  (defn total-score [self i j]
    (assert (= (len self.my-history) (len self.op-history)))
    (assert (<= (len self.my-history) j))
    (assert (<= i j))
    (setv total 0)
    (for [n (range i j)]
      (+= total (turn-score (. self.my-history [n])
                            (. self.op-history [n]))))
    total)

  (defn --init-- [self &optional [round 0]]
    (assert (!= round 0) f"round id 0")
    (setv self.my-history []
          self.op-history []
          self.round round
          self.turn 0)
    
    )

  (defn move [self &optional [previous None]]
    (+= self.turn 1)
    (if (in "handshakeid" (get-opponent-source self))
        (return (+ 2 (% (inc self.turn) 2))))

    (lif previous
         (.append self.op-history previous)
         (do (.append self.my-history 2)
             (return 2)))
    (setv output
          (if (and (> (+ (last self.my-history) previous)
                      5)
                   (> previous 2))
              5
              (= 2 (last self.my-history)) 3
              (= 3 (last self.my-history)) 2
              (= 5 (last self.my-history)) (if (<= 2 previous) 2 5)
              3))

    (.append self.my-history output)
    output))
