(import [random [randint]])
(defclass CooperateBot []
  (defn --init-- [self &optional [round 0]]
    )
  (defn move [self &optional [previous None]]
    (setv olm previous)
    (setv self.mlm
          (lif previous
               (if (= (+ olm self.mlm) 5)
                   olm
                   (if (randint 0 1)
                       (max [self.mlm olm])
                       (- 5 (max [self.mlm olm]))))
               2))
    self.mlm))
