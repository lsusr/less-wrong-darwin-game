(defclass CooperateBot []
  (defn --init-- [self &optional [round 0]]
    (assert (!= round 0)
            "There is a bug. The game engine does not start rounds at 0")
    (setv self.turn 0)
    (setv self.opponent-max None))
  (defn move [self &optional [previous None]]
    (+= self.turn 1)
    (if (= None self.opponent-max)
        (setv self.opponent-max previous))
    (if (<= self.turn 10)
        3
        (max [3 self.opponent-max]))))
