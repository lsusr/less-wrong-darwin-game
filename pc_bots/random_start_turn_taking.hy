(import random)
(defclass RandomStartTurnTaking []
  (defn --init-- [self &optional [round 1]]
    (setv self.previous None))
  (defn move [self &optional [previous None]]
    (setv output
          (lif previous
               (if (or (and (= self.previous 2)
                            (<= previous 2))
                       (and (= self.previous 3)
                            (>= 3 previous)))
                   (random.randint 2 3)
                 (if (<= previous 2)
                     2
                     3))
               (random.randint 2 3)))
    (setv self.previous output)
    output))
