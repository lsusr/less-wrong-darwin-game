class OscillatingTwoThreeBot():
    def __init__(self, round=1):
        self.turn = 0
        self.round = round
        self.previous = None
        self.LWName = "Tetraspace Grouping"
        self.funNumbers = 9876543210
    def move(self, previous=None):
        self.turn += 1
        output = 2 + self.turn % 2
        return output
