;; Number of rounds so far is (len self.opponent-history)

(defn subsequence-in? [subsequence sequence]
  ;; only guaranteed to work for lists
  (if (> (len subsequence) (len sequence))
      False
      (= subsequence (cut sequence 0 (len subsequence)))
      True
      (subsequence-in subsequence (cut sequence 1))))

(defclass Empiricist []
  (defn compute-total-scores [self]
    (assert (= (len self.my-history) (len self.opponent-history)))
    (setv my-score 0 opponent-score 0)
    (for [i (range (len self.my-history))]
      (if (<= (+ (. self.my-history [i]) (. self.opponent-history [i]))
              5)
          (do
            (+= my-score (. self.my-history [i]))
            (+= opponent-score (. self.opponent-history [i])))))
    (tuple [my-score opponent-score]))

  (defn --init-- [self &optional [round 0]]
    (setv self.round round
          self.my-history []
          self.opponent-history []))

  (defn move [self &optional [previous None]]
    (lif previous
         (.append self.opponent-history previous))
    (setv output
          (lif previous
               (do
                 (setv [my-score opponent-score] (.compute-total-scores self))
                 (if (> opponent-score (+ 5 my-score))
                     3
                     (do
                       (setv m 0)
                       (while (.last-m-match? self :m m)
                         (+= m 1))
                       (if (= m 0)
                           previous
                           (do
                             (setv start-index 0)
                             (while
                               (and
                                 (subsequence-in?
                                   :subsequence (cut self.my-history (* -1 m))
                                   :sequence (cut self.my-history start-index
                                                  (* -1 m)))
                                 (subsequence-in?
                                   :subsequence (cut self.opponent-history (* -1 m))
                                   :sequence (cut self.opponent-history start-index
                                                  (* -1 m)))))
                             (setv y (. self.opponent-history [start-index]))
                             (if (< y 5)
                                 (- 5 y)
                                 2))))))
               2))
    (.append self.my-history output)
    output)

  (defn last-m-match? [self m]
    (if (> m (len self.my-history))
        (return False))
    (and
      (subsequence-in? :subsequence (cut self.my-history (* -1 m))
                       :sequence (cut self.my-history 0 (* -1 m)))
      (subsequence-in? :subsequence (cut self.opponent-history (* -1 m))
                       :sequence (cut self.opponent-history 0 (* -1 m))))))
