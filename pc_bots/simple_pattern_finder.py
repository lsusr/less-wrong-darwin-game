import random

class SimplePatternFinderBot():
    
  
    def __init__(self, round=1):
        self.turn = 0
        self.round = round
        
        self.ourScore = 0
        self.theirScore = 0
        
        self.ourPrevious = 0
        
        self.ourMoves = []
        self.theirMoves = []
        
        self.theirHighestPlayed = 0
        
        self.hasTotalOfFiveBeenPlayed = False
        
        
    # Holds data for a detected pattern
    class PatternData():
        
        def __init__(self, patternLength, theirMoveList):
           
            self.theirPattern = []
            self.ourScoreWithOppositePattern = 0
            self.theirScoreWithPattern = 0
           
            for move in range (0, patternLength):
                self.theirPattern.append(theirMoveList[\
                len(theirMoveList) - patternLength + move])
                   
            for move in self.theirPattern:
                self.theirScoreWithPattern += move
                self.ourScoreWithOppositePattern += 5 - move
               
               
        def IsPatternFairOrGoodForUs(self):
            return self.ourScoreWithOppositePattern >=\
            self.theirScoreWithPattern
        
        # Returns the next number it will play if it sticks to the pattern
        def GetNext(self):
            return self.theirPattern[0]
        
        # Returns what we would play next if we are sticking with
        # the pattern
        def OurNext(self):
            return 5 - self.theirPattern[0]
    
    # This tries to find a simple pattern that is being played by the 
    # other bot. If it thinks it has found one it returns its best guess
    # as to what it will play next. Otherwise it returns -1
    def EvaluateSimplePattern(self):
        
        ret = None
        
        noMoves = len(self.theirMoves)
        gapList = [6, 5, 4, 3, 2, 1]
        noToCheck = 4
    
        for gap in gapList:
            
            startIdx = self.turn - 1 - noToCheck * gap
            
            if startIdx < 0:
                break
            
            patternMaintained = True
            for testMove in self.theirMoves[startIdx: startIdx + gap]:
                    
                nextIdx = startIdx + gap
                while nextIdx < noMoves:
                    
                    if self.theirMoves[startIdx] != \
                       self.theirMoves[nextIdx]:
                        patternMaintained = False
                        break
                    nextIdx += gap
                
                if not patternMaintained:
                    break
                              
            # We have found a pattern
            if patternMaintained:
                ret = self.PatternData(gap, self.theirMoves)
                break

        return ret   
    

    def move(self, previous=None):
        self.turn += 1
        ret = -1
        
        # Update our data store
        if self.turn > 1:
            if previous + self.ourPrevious <= 5:
                self.ourScore += self.ourPrevious
                self.theirScore += previous
                
            self.theirMoves.append(previous)
            self.ourMoves.append(self.ourPrevious)
            
            if previous + self.ourPrevious == 5:
                self.hasTotalOfFiveBeenPlayed = True
    
            # See if we can find a pattern
            pattern = self.EvaluateSimplePattern()
            
            if pattern != None:
                if pattern.IsPatternFairOrGoodForUs():
                    # Try and stick to it as it looks good
                    ret = pattern.OurNext()
                else:
                    # We have a problem. If it is a smart opponent we
                    # don't want to encourage it to stick with it, on
                    # the other hand if it is a dumb bot that will stick
                    # with it regardless then we are better getting
                    # something rather than nothing. It's also possible
                    # we might not have been able to establish
                    # co-operation yet
                    
                    if pattern.OurNext() >= 3:
                        # The pattern is godd for us for at least this
                        # move, so stick to it for now.
                        ret = pattern.OurNext()
                    elif (self.theirScore + pattern.GetNext())/self.turn\
                        >= 2.25:
                        # Under no circumstances allow it to get too many
                        # points from playing an unfavourable pattern
                        ret = 3
                    elif self.theirMoves[-1] + self.ourMoves[-1] == 5:
                        # If we managed to co-operate last round,
                        # hope we can break the pattern and co-operate
                        # this round.
                        return self.theirMoves[-1]
                    elif not self.hasTotalOfFiveBeenPlayed:
                        # If the combined scores have never been 5
                        # before try to arrange this to see if it will
                        # break the deadlock.
                        ret = pattern.OurNext()
                    elif self.round < 4 and pattern.OurNext() >= 1:
                        # It looks like we are probably dealing with
                        # a nasty bot. Tolerate this within limits in
                        # the early game where it is more likely to be
                        # a dum bot than a sophisticated bot that is
                        # very good at exploiting us, so we at least
                        # get something
                        ret = pattern.OurNext()
                    elif self.round < 8 and pattern.OurNext() >= 2:
                        # If we would get an extra point be tolerant
                        # for a little longer.
                        ret = pattern.OurNext()
                    else:
                        # It looks like it is being completly
                        # unreasonable, so normally return 3
                        # to stop us from being exploited,
                        # but occasionally offer 2 just in case
                        # we have managed to accidentally get
                        # ourselves into a defect cycle against
                        # a more reasonable bot
                        num = random.randint(0,50)
                        if num == 0:
                            # Possibly this should only be done once?
                            ret = 2
                        else:
                            ret = 3
                    
                
        if ret < 0 and self.turn > 1:
            # We have been unable to establish any kind of pattern            
            if self.ourPrevious + previous == 5:
                # Hope to establish a co-operative pattern
                ret = previous
            elif self.ourPrevious + previous < 5:
                # Try to get some of the remaining points without
                # appearing too greedy.
                # In testing this appeared to work better than always
                # grabbing some extra points for ourself.
                pointsAvailable = 5 - self.ourPrevious - previous
                ret = self.ourPrevious + random.randint(0, pointsAvailable)
            else:
                # Continue trying random numbers
                ret = random.randint(2,3)
        elif ret < 0:
            # This is the first turn
            # Avoid playing too high/low a number to reduce the chances
            # of looking like a bully or a weak bot.
            # A bit of randomness is probably a bit less exploitable
            ret = random.randint(2,3)
        
        # These lines might just confuse some simple bots that try to
        # evaluate our source code
        # from extra import get_opponent_source
        # dummy = get_opponent_source()
            
            
        self.ourPrevious = ret
        return ret
