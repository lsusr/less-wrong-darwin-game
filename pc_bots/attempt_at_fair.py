class AttemptAtFair():
    def __init__(self, round=0):
        self.turn = 0
        self.round = round
        self.previous = None
    def move(self, previous=None):
        self.turn += 1
        if self.turn % 2 == 1:
            return 3
        else:
            return 2
