(import random)
(setv +sequence-1+ [2 3 3 2 2 2 3 2 3 2 3 3 2 2 2 3 3])
(setv +sequence-2+ [2 2 2 2 2 2 2 3])

(defclass BendBot []
  (defn --init-- [self &optional [round 0]]
    (setv self.round round)
    (setv self.opponent-record [])
    (setv self.record [])
    (setv self.turn -1)
    (setv self.count-to-four 0)
    (setv self.terminal-behavior False))

  (defn move [self &optional [previous None]]
    (+= self.turn 1)
    (if previous
        (.append self.opponent-record previous))
    (setv output
          (if (= self.opponent-record self.record)
              (if (or (% self.turn 2)
                      (< self.turn 4))
                  (if (< self.turn (len +sequence-1+))
                      (. +sequence-1+ [self.turn])
                      (. +sequence-2+ [(% (- (len +sequence-1+)
                                             self.turn)
                                          (len +sequence-2+))]))
                  (if (< self.turn (len +sequence-1+))
                      (. (list (reversed +sequence-1+)) [self.turn])
                      (. +sequence-2+ [(% (- (len +sequence-1+)
                                             self.turn)
                                          (len +sequence-2+))])))
              (if (< self.count-to-four 4)
                  (do
                    (+= self.count-to-four 1)
                    (if (= (last self.record) 3)
                        2
                        3))
                  (if (or self.terminal-behavior
                          (= 5 (+ (last self.record) previous)))
                      (if (= (last self.record) 3)
                          2
                          3)
                      (do
                        (setv self.terminal-behavior True)
                        (if (all (map (fn [x] (<= x 0)) self.opponent-record))
                            5
                            (all (map (fn [x] (<= x 1)) self.opponent-record))
                            4
                            (all (map (fn [x] (<= x 2)) self.opponent-record))
                            3
                            (if (> (+ (last self.record)
                                      previous)
                                   5)
                                3
                                2)
                            ))))))
    (.append self.record output)
    output))
