(import random)
(defclass S_A []
  (defn --init-- [self &optional [round 0]])
  (defn move [self &optional [previous None]]
    (setv choice (random.randint 1 100))
    (if (<= choice 1)
        (random.randint 0 5)
        (<= 80)
        1
        5)))
