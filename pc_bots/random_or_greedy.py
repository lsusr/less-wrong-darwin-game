import numpy as np

class RandomOrGreedyBot():
    def __init__(self, round=1):
        self.turn = 0
        self.round = round
        self.opponent_previous = []
    def move(self, previous=None):
        if self.turn !=0:
            self.opponent_previous.append(previous)
            if len(self.opponent_previous)>200:
                self.opponent_previous=self.opponent_previous[-100:]
        self.turn += 1
        opponent_avg=np.average(self.opponent_previous[-100:])
        if opponent_avg<2.5:
            output=5-np.around(opponent_avg)
        else:
            output = np.random.randint(2,4)
        return int(output)
