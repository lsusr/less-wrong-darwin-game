class BeauBot():
    # bot attempts to cooperate and will even risk three points here and there in early rounds
    # if other bot defects in later rounds, BeauBot will bid 4's until other bot bids 1
    def __init__(self, round=0):
        self.round = round
        self.turn = 0
        self.myPrevious = None
        self.theirPrevious = None
        self.strategy = None

    def move(self, previous=None):
        # sets starting bid to be 2 in before round 10,
        # alternating 2 and 3 between rounds 10 and 30, and 3 in later rounds
        self.turn += 1
        self.theirPrevious = previous
        if self.theirPrevious == None:
            if self.round < 10:
                self.myPrevious = 2
                return self.myPrevious
            elif self.round < 30:
                self.myPrevious = self.round % 2 + 2
                return self.myPrevious
            else:
                self.myPrevious = 3
                return self.myPrevious
        else:
            self.myPrevious = self.strategyPicker()
            return self.myPrevious

    def strategyPicker(self):
        # chooses between 3 simple strategies to use based on current strategy and previous bids
        if self.theirPrevious == 1 or self.theirPrevious == 2 and self.strategy != 'hard':  # will always cooperate if other bot bids low if not using hard strategy
            return self.titForTat()
        elif self.theirPrevious == 3 or self.strategy == 'titForTat':                       # if in tit for tat strategy, will allow higher numbers to be alternated
            if self.strategy == 'titForTat' or (self.myPrevious == 3 and self.round < 11):  # will only cooperate with 3 bids if self.myPrevious was 3
                return self.titForTat()
            else:
                if self.round < 5:                                                          # bot will be try to get points from aggressors in early rounds
                    self.strategy = 'soft'
                    return self.soft()
                else:
                    self.strategy = None
                    return 3
        else:
            if self.round < 5:
                self.strategy = 'soft'
                return self.soft()
            else:
                self.strategy = 'hard'
                return self.hard()

    def titForTat(self):
        #will copy opponent move if self.theirPrevious sum = 5, will try to fix if bids not adding to 5,
        #will switch out of tit for tat  if opponent continues high bids
        if self.theirPrevious + self.myPrevious == 5:                                        # could allow for 4-1 alternating bids only if opponent bids 1 when bot was bidding 4 (from hard strategy)
            self.strategy = 'titForTat'
            return self.theirPrevious
        elif self.theirPrevious + self.myPrevious < 5:
            self.strategy = 'titForTat'
            return self.theirPrevious + self.turn % 2                                        # only tries to correct every other round in case other bot is also correcting for underbiding in cooperation
        elif self.theirPrevious + self.myPrevious > 5:                                       # will not cooperate will not continue tit for tat if a 3 bid is made after failing to cooperate in the self.theirPrevious round
            self.strategy = None
            if self.myPrevious > 2:
                return 2
            else:
                if self.round < 5:
                    self.strategy = 'soft'
                    return self.soft()
                else:
                    self.strategy = 'hard'
                    return self.hard()

    def soft(self):
        #will bid 1 or 2 if opponent bids 4 or 3 respectively, only to be used in early rounds
        #intended to get early growth by feeding bullies and attackers early
        # will switch to titFortat if other bot bids low
        if self.theirPrevious > 2 and self.theirPrevious < 5:
            return 5 - self.theirPrevious
        elif self.theirPrevious < 3:
            self.strategy = 'titForTat'
            return self.theirPrevious
        return 1


    def hard(self):
        # will return 4 as long as opponent does not return 1
        # if opponent returns 1, the extra 4 points should be enough buffer to attempt cooperation again
        if self.theirPrevious == 1:
            self.strategy = 'titForTat'
            return 2
        else:
            self.strategy == 'hard'
            return 4
