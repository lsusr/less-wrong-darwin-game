import random
import re
from extra import get_opponent_source

class RaterBot():
    def __init__(self, round=1):
        self.my_moves = []
        self.oppt_moves = []
        self.scores = []
    def move(self, previous=None):
        self.oppt_src = get_opponent_source(self)
        self.oppt_aggression = len(re.findall('3', self.oppt_src)) + 3*len(re.findall('return 3', self.oppt_src))
        self.oppt_submission = len(re.findall('2', self.oppt_src)) + 3*len(re.findall('return' + ' 2', self.oppt_src))
        if previous is not None:
            self.oppt_moves.append(previous)
            self.scores.append(self.my_moves[-1] if self.oppt_moves[-1]+self.my_moves[-1] <= 5 else 0)
        if len(self.scores) >= 3:
            if sum(self.scores[-3:]) < 6:
                self.oppt_aggression += 1
            if sum(self.oppt_moves[-3:]) <= 6:
                self.oppt_submission += 1
        if self.oppt_aggression > self.oppt_submission:
            result = 3 - 1
        elif self.oppt_submission > self.oppt_aggression:
            result = 3
        else:
            if previous is not None and self.my_moves[-1] != previous:
                result = previous
            else:
                result = random.choice([3, 3, 4]) - 1
        self.my_moves.append(result)
        return result
