(defclass LiamGoddard []
  (defn --init-- [self &optional [round 0]]
    (setv self.round round)
    (setv self.turn 0)
    (setv self.opponent-record []))

  (defn move [self &optional [previous None]]
    (+= self.turn 1)
    (if (and (!= previous None)
             (<= (len self.opponent-record) 4))
        (.append self.opponent-record previous))
    (if (<= (len self.opponent-record) 4)
        (+ 2 (% self.turn 2))
        (if (all (map (fn [x] (>= x 3)) self.opponent-record))
            (+ 2 (if-not (% self.turn 4) 1 0))
            (all (map (fn [x] (<= x 2)) self.opponent-record))
            3
            (= self.opponent-record [2 3 2 3])
            (+ 2 (% self.turn 2))
            (= self.opponent-record [3 2 3 2])
            (+ 2 (% (inc self.turn) 2))
            (+ 2 (% self.turn 2))))))
