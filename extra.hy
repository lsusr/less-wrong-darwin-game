#!/home/lsusr/.local/bin/hy

(setv +CR+ "
")

(defn get-opponent-source [self]
  (setv output-set 
        (- (get-bot-classes)
           (set [(type self)])))
  (assert (= (len output-set) 1) f"output-set is {output-set}")
  (setv target-class (first output-set))
  (with [f (open (inspect.getfile target-class) "r")]
    (.read f)))


(defn get-my-source [self]
  (with [f (open (inspect.getfile (type self)) "r")]
    (.read f)))

(import datetime inspect os random)

;;;;;;;;;;
;; bots ;;
;;;;;;;;;;

(import [npc-bots [zero one two three four five chaos tft invert cement spy]]
        [pc-bots [walcb s_a random_or_greedy oscillating_two_three_bot abstract_spy_tree_bot pure_tft random_start_turn_taking rater bend copoperator liam_goddard empiricist attempt_at_fair copy_deluxe wcwajga cooperate1 cooperate2 measure beau simple_pattern_finder password1 password2 early_bird_mimic_bot jacob ben clone1_karma clone2_zvi clone3 clone4_a_comatose_squirrel clone5_very_social clone6_incomprehensibot clone7_akrasia clone8_clone_wars]])
(setv bots (| (set [zero.ZeroBot
                    one.OneBot
                    two.TwoBot
                    three.ThreeBot
                    four.FourBot
                    five.FiveBot
                    chaos.ChaosBot
                    tft.TitForTatBot2
                    tft.TitForTatBot3
                    invert.InvertBot2
                    invert.InvertBot3
                    invert.InvertBot1
                    invert.InvertBot4
                    invert.InvertBot0
                    invert.InvertBot5
                    invert.CounterInvertBot
                    invert.RandomInvertBot
                    invert.RandomInvertBot23
                    cement.CementBot2
                    cement.CementBot3
                    cement.TwoOrThreeCement
                    #_spy.SpyBot])
              #_(set [
                    #_early_bird_mimic_bot.CloneBot
                    clone1_karma.CloneBot
                    clone2_zvi.CloneBot ;; this bot is not associated with the Less Wrong user Zvi
                    clone3.CloneBot
                    clone4_a_comatose_squirrel.CloneBot
                    clone5_very_social.CloneBot
                    clone6_incomprehensibot.CloneBot
                    clone7_akrasia.CloneBot
                    clone8_clone_wars.CloneBot
                    ]
                   )
              (set 
                [walcb.WinnerAgainstLowConstant
                  s_a.S_A
                  random_or_greedy.RandomOrGreedyBot
                  oscillating_two_three_bot.OscillatingTwoThreeBot
                  abstract_spy_tree_bot.AbstractSpyTreeBot
                  pure_tft.PureTFT
                  random_start_turn_taking.RandomStartTurnTaking
                  rater.RaterBot
                  bend.BendBot
                  liam_goddard.LiamGoddard
                  copoperator.Cooperator
                  empiricist.Empiricist
                  attempt_at_fair.AttemptAtFair
                  copy_deluxe.CopyBotDeluxe 
                  wcwajga.Wcwajga
                  cooperate1.CooperateBot
                  cooperate2.CooperateBot
                  measure.MeasureBot
                  beau.BeauBot
                  simple_pattern_finder.SimplePatternFinderBot
                  password1.PasswordBot
                  password2.PasswordBot
                  early_bird_mimic_bot.CloneBot
                  ben.BenBot
                  jacob.JacobBot
                  clone1_karma.CloneBot
                  clone2_zvi.CloneBot ;; this bot is not associated with the Less Wrong user Zvi
                  clone3.CloneBot
                  clone4_a_comatose_squirrel.CloneBot
                  clone5_very_social.CloneBot
                  clone6_incomprehensibot.CloneBot
                  clone7_akrasia.CloneBot
                  clone8_clone_wars.CloneBot])))

(setv +ordered-bots+ (sorted bots :key (fn [x] (str x))))
(setv +output-path+
      (os.path.join "csvs" (+ (.isoformat (.now datetime.datetime)) ".csv")))
(setv +details-path+
      (os.path.join "csvs" (+ (.isoformat (.now datetime.datetime)) "-details.csv")))
(assert (not (os.path.exists +output-path+)))

;;;;;;;;;;;;;;;;;;;
;; configuration ;;
;;;;;;;;;;;;;;;;;;;

(setv +legal-moves+ (set (range (inc 5)))
      +num-rounds+ 10000
      +iterations-per-round+ 100
      +seed-population+ 100)

;;;;;;;;;;;;;;;
;; variables ;;
;;;;;;;;;;;;;;;

(setv populations (dfor bot bots [bot +seed-population+]))

;;;;;;;;;;;;;;;
;; functions ;;
;;;;;;;;;;;;;;;

(defn get-bot-classes []
  game.bot-classes)

(defn queue-from-populations [populations]
  (setv queue (flatten (lfor [bot population]
                             (.items populations)
                             (* population [bot]))))
  (random.shuffle queue)
  queue)

(defn game [bot-class-0 bot-class-1 round-index]
  (if (= bot-class-0 bot-class-1)
      (* 2 [(* 2.5 +iterations-per-round+)])
      (do
        (setv score [0 0])
        (setv game.bot-classes (set [bot-class-0 bot-class-1]))
        (assert (hasattr game "bot_classes"))
        (setv bot0 (bot-class-0 :round round-index)
              bot1 (bot-class-1 :round round-index))
        (setv bot0.opponent-source
              (with [f (open (inspect.getfile bot-class-1) "r")]
                (.read f)))
        (setv bot1.opponent-source
              (with [f (open (inspect.getfile bot-class-0) "r")]
                (.read f)))
        (setv bot0.my-source bot1.opponent-source)
        (setv bot1.my-source bot0.opponent-source)
        (setv previous-moves [None None])
        (for [_ (range +iterations-per-round+)]
          (setv bot0-move (.move bot0 (second previous-moves)))
          (setv bot1-move (.move bot1 (first previous-moves)))
          (assert (in bot0-move +legal-moves+)
                  f"Illegal move {bot0-move} by {bot-class-0}")
          (assert (in bot1-move +legal-moves+)
                  f"Illegal move {bot1-move} by {bot-class-1}")
          (if (<= (+ bot0-move bot1-move) 5)
              (do (+= (. score [0]) bot0-move)
                  (+= (. score [1]) bot1-move)))
          (setv previous-moves (tuple [bot0-move bot1-move])))
        (with [f (open +details-path+ "a")]
          (.write f
                  (+
                    (.join ","
                           [(str round-index)
                            (str bot0)
                            (str bot1)
                            (str (. score [0]))
                            (str (. score [1]))])
                    +CR+)))
        score)))

(defn populations-from-scores [scores]
  (setv total-score (sum (.values scores)))
  (setv total-population (* +seed-population+ (len bots)))
  (setv scaling-factor (/ total-population total-score))
  #_"The total population may deviate slightly from the target population
     due to rounding errors."
  (dfor [bot score]
        (.items scores)
        [bot (round (* score scaling-factor))]))

#_(setv populations (dfor bot bots [bot +seed-population+]))
(defn pretty-print [populations index-str]
  (print f"POPULATIONS {index-str}")
  (with [f (open +output-path+ "a")]
    (.write f (+ (.join "," (map (fn [c] (str (. populations [c])))
                                 +ordered-bots+))
                 +CR+)))
  (for [[bot pop] (.items populations)]
    (if pop
        (print f"Bot {bot} pop {pop}")))
  (if (= 1 (len populations))
      (exit)))

;; main game loop
;; wrapping this in a main function doesn't work
(with [f (open +output-path+ "w")]
  (.write f (+ (.join "," (map str +ordered-bots+))
               +CR+)))
(with [f (open +details-path+ "w")]
  (.write f (+ "round,bot0,bot1,score0,score1"
               +CR+)))
(for [r (range +num-rounds+)]
  (setv queue (queue-from-populations populations))
  (setv scores (dfor bot bots [bot 0]))
  ;; If the queue contains an odd number of elements, the last entry will be ignored.
  (for [i (range (// (len queue) 2))]
    (setv index (* 2 i))
    (setv bot0-class (. queue [i])
          bot1-class (. queue [(inc i)]))
    (setv game-scores (game bot0-class bot1-class :round-index (inc r)))
    (+= (. scores [bot0-class]) (first game-scores))
    (+= (. scores [bot1-class]) (second game-scores)))
  (setv populations (populations-from-scores scores))
  (for [k (.keys populations)]
    (if (<= (. populations [k]) 2)
        (setv (. populations [k]) 0)))
  (pretty-print populations (str r)))
