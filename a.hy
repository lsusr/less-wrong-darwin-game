#!/home/lsusr/.local/bin/hy

(import [matplotlib.pyplot :as plt]
        [numpy :as np])

(import [pandas :as pd])

;(setv +data-path+ "csvs/2020-10-25T19:18:47.752901-measure.csv")
;(setv +data-path+ "csvs/2020-10-25T20:05:57.510588-mimic.csv")
;(setv +data-path+ "csvs/2020-10-25T20:27:45.160439-jacob.csv")
;(setv +data-path+ "csvs/2020-11-10T13:55:39.015519.csv") ;; official game
;(setv +data-path+ "csvs/2020-11-10T15:00:18.595068.csv") ;; official game
;(setv +data-path+ "csvs/2020-11-10T15:21:20.816096.csv") ;; multicore and the clones
;(setv +data-path+ "csvs/2020-11-10T15:36:38.622265.csv")
;(setv +data-path+ "csvs/2020-11-10T15:36:38.622265.csv")
;(setv +data-path+ "csvs/2020-11-13T18:20:50.226626.csv")
;(setv +data-path+ "csvs/2020-11-13T18:20:50.226626.csv")
(setv +data-path+ "csvs/2020-11-29T13:20:10.800909.csv")

(setv +class-str-to-name+
      {"<class 'npc_bots.cement.CementBot2'>" "Silly Cement Bot 2"
       "<class 'npc_bots.cement.CementBot3'>" "Silly Cement Bot 3"
       "<class 'npc_bots.cement.TwoOrThreeCement'>" "Silly Cement Bot 2-3"
       "<class 'npc_bots.chaos.ChaosBot'>" "Silly Chaos Bot"
       "<class 'npc_bots.five.FiveBot'>" "Silly 5 Bot"
       "<class 'npc_bots.four.FourBot'>" "Silly 4 Bot"
       "<class 'npc_bots.invert.CounterInvertBot'>" "Silly Counter Invert Bot"
       "<class 'npc_bots.invert.InvertBot0'>" "Silly Invert Bot 0"
       "<class 'npc_bots.invert.InvertBot1'>" "Silly Invert Bot 1"
       "<class 'npc_bots.invert.InvertBot2'>" "Silly Invert Bot 2"
       "<class 'npc_bots.invert.InvertBot3'>" "Silly Invert Bot 3"
       "<class 'npc_bots.invert.InvertBot4'>" "Silly Invert Bot 4"
       "<class 'npc_bots.invert.InvertBot5'>" "Silly Invert Bot 5"
       "<class 'npc_bots.invert.RandomInvertBot'>" "Silly Random Invert Bot"
       "<class 'npc_bots.invert.RandomInvertBot23'>" "Silly Random Invert Bot 2-3"
       "<class 'npc_bots.one.OneBot'>" "Silly 1 Bot"
       "<class 'npc_bots.tft.TitForTatBot2'>" "Silly TFT Bot 2"
       "<class 'npc_bots.tft.TitForTatBot3'>" "Silly TFT Bot 3"
       "<class 'npc_bots.three.ThreeBot'>" "Silly 3 Bot"
       "<class 'npc_bots.two.TwoBot'>" "Silly 2 Bot"
       "<class 'npc_bots.zero.ZeroBot'>" "Silly 0 Bot"
       "<class 'pc_bots.abstract_spy_tree_bot.AbstractSpyTreeBot'>" "AbstractSpyTreeBot"
       "<class 'pc_bots.attempt_at_fair.AttemptAtFair'>" "AttemptAtFair"
       "<class 'pc_bots.beau.BeauBot'>" "BeauBot"
       "<class 'pc_bots.ben.BenBot'>" "Ben-Bot"
       "<class 'pc_bots.bend.BendBot'>" "BendBot"
       "<class 'pc_bots.clone1_karma.CloneBot'>" "KarmaBot"
       "<class 'pc_bots.clone2_zvi.CloneBot'>" "CliqueZviBot"
       "<class 'pc_bots.clone3.CloneBot'>" "CloneBot"
       "<class 'pc_bots.clone4_a_comatose_squirrel.CloneBot'>" "a_comatose_squirrel"
       "<class 'pc_bots.clone5_very_social.CloneBot'>" "A Very Social Bot"
       "<class 'pc_bots.clone6_incomprehensibot.CloneBot'>" "incomprehensibot"
       "<class 'pc_bots.clone7_akrasia.CloneBot'>" "Akrasia Bot"
       "<class 'pc_bots.clone8_clone_wars.CloneBot'>" "Clone wars, episode return 3"
       "<class 'pc_bots.cooperate1.CooperateBot'>" "CooperateBot [Insub]"
       "<class 'pc_bots.cooperate2.CooperateBot'>" "CooperateBot [Larks]"
       "<class 'pc_bots.copoperator.Cooperator'>" "Copoperater"
       "<class 'pc_bots.copy_deluxe.CopyBotDeluxe'>" "CopyBot Deluxe"
       "<class 'pc_bots.early_bird_mimic_bot.CloneBot'>" "EarlyBirdMimicBot"
       "<class 'pc_bots.empiricist.Empiricist'>" "Empiricist"
       "<class 'pc_bots.jacob.JacobBot'>" "jacobjacob-Bot"
       "<class 'pc_bots.liam_goddard.LiamGoddard'>" "LiamGoddard"
       "<class 'pc_bots.measure.MeasureBot'>" "MeasureBot"
       "<class 'pc_bots.oscillating_two_three_bot.OscillatingTwoThreeBot'>" "OscillatingTwoThreeBot"
       "<class 'pc_bots.password1.PasswordBot'>" "PasswordBot"
       "<class 'pc_bots.password2.PasswordBot'>" "Definitely Not Collusion Bot"
       "<class 'pc_bots.pure_tft.PureTFT'>" "Pure TFT"
       "<class 'pc_bots.random_or_greedy.RandomOrGreedyBot'>" "RandomOrGreedyBot"
       "<class 'pc_bots.random_start_turn_taking.RandomStartTurnTaking'>" "Random-start-turn-taking"
       "<class 'pc_bots.rater.RaterBot'>" "RaterBot"
       "<class 'pc_bots.s_a.S_A'>" "S_A"
       "<class 'pc_bots.simple_pattern_finder.SimplePatternFinderBot'>" "SimplePatternFinderBot"
       "<class 'pc_bots.walcb.WinnerAgainstLowConstant'>" "Winner against low constant bots"
       "<class 'pc_bots.wcwajga.Wcwajga'>" "Why can't we all just get along"})

(setv +name-to-color+
      {"Silly Cement Bot 2" "m"
       "Silly Cement Bot 3" "m"
       "Silly Cement Bot 2-3" "m"
       "Silly Chaos Bot" "m"
       "Silly 5 Bot" "m"
       "Silly 4 Bot" "m"
       "Silly Counter Invert Bot" "m"
       "Silly Invert Bot 0" "m"
       "Silly Invert Bot 1" "m"
       "Silly Invert Bot 2" "m"
       "Silly Invert Bot 3" "m"
       "Silly Invert Bot 4" "m"
       "Silly Invert Bot 5" "m"
       "Silly Random Invert Bot" "m"
       "Silly Random Invert Bot 2-3" "m"
       "Silly 1 Bot" "m"
       "Silly TFT Bot 2" "m"
       "Silly TFT Bot 3" "m"
       "Silly 3 Bot" "m"
       "Silly 2 Bot" "m"
       "Silly 0 Bot" "m"
       "AbstractSpyTreeBot" "black"
       "AttemptAtFair" "black"
       "BeauBot" "black"
       "Ben-Bot" "g"
       "BendBot" "black"
       "KarmaBot" "b"
       "CliqueZviBot" "b"
       "CloneBot" "b"
       "a_comatose_squirrel" "b"
       "A Very Social Bot" "b"
       "incomprehensibot" "b"
       "Akrasia Bot" "b"
       "Clone wars, episode return 3" "b"
       "CooperateBot [Insub]" "black"
       "CooperateBot [Larks]" "black"
       "Copoperater" "black"
       "CopyBot Deluxe" "black"
       "EarlyBirdMimicBot" "r"
       "Empiricist" "black"
       "jacobjacob-Bot" "g"
       "LiamGoddard" "black"
       "MeasureBot" "black"
       "OscillatingTwoThreeBot" "black"
       "PasswordBot" "r"
       "Definitely Not Collusion Bot" "r"
       "Pure TFT" "black"
       "RandomOrGreedyBot" "black"
       "Random-start-turn-taking" "black"
       "RaterBot" "black"
       "S_A" "black"
       "SimplePatternFinderBot" "black"
       "Winner against low constant bots" "black"
       "Why can't we all just get along" "black"})

(setv df (pd.read-csv +data-path+))

(setv (. df columns) (map (fn [col-name] (. +class-str-to-name+ [col-name]))
                          (. df columns)
                          ))
#_(setv df (pd.DataFrame (. df ["A Very Social Bot"]))) ;; just show one bot

(do
  (setv turns-of-survival
        (dfor column (. df columns)
              [column
               (last (. (np.trim-zeros (.squeeze (. df [[column]])))
                        index))]))
  (for [k (sorted (.keys turns-of-survival)
                  :key (fn [x] (. turns-of-survival [x])))]
    (setv n (str (inc (. turns-of-survival [k]))))
    (print f"\"{k}\" died on turn {n}")))

(defn show-slice [begin end]
  (show-slice-on-plot :plot plt :begin begin :end end)
  (.show plt))

(defn show-slice-on-plot [plot begin end]
  ;(.yscale plot "log")
  (for [column (. df columns)]
    (setv series (np.trim-zeros (.squeeze (cut (. df [[column]] iloc)
                                               begin (inc end))))
          color (. +name-to-color+ [column]))
    (plot.plot
      series
      :label "three"
      :color color
      )
    (try
      (.set-title plot f"Rounds {begin} to {end}")
      (except [AttributeError]))
    (if (and (.any series.index)
             (> (last series.index) begin))
        (plot.annotate column (tuple [(last series.index) (last series)])
                      :color color)))

  )

(defn show-pie-on-plot [plot n]
  (setv row (. df iloc [n])
        labels [] sizes []
        colors [])
  (setv ordered-rows (sorted (.items row)
                             :key (fn [pair] (. +name-to-color+ [(first pair)]))
                             ))
  (for [[bot score] ordered-rows]
    (if score
        (do
          (.append labels bot)
          (.append sizes score)
          (.append colors (. +name-to-color+ [bot])))))
  ;(setv [fig1 ax1] (.subplots plot))
  (try
    (.set-title plot f"Round {n}")
    (except [AttributeError]))
  (.pie plt sizes :labels labels :colors colors :startangle 90)
  (.axis plt "equal"))

(defn show-pie [n]
  (show-pie-on-plot :plot plt :n n)
  (.savefig plt f"/home/lsusr/temp/pie-{n}.png" :bbox-inches "tight")
  (.show plt))

(defn bar-with-pie [begin end]
  "The pie chart of round end will be shown"
  (setv [fig axes] (.subplots plt :nrows 1 :ncols 2 :figsize (tuple [5 3])))
  (show-slice-on-plot :plot (. axes [0]) :begin begin :end end)
  (show-pie-on-plot :plot (. axes [1]) :n end)
  (.subplots-adjust plt :wspace 1)
  (.show plt))

;(print (. df iloc [30]))
;(show-pie 1)
;(bar-with-pie 0 2)
;(bar-with-pie 30 100)
;(bar-with-pie 100 500)

;(show-slice 0 10)
;(show-slice 0 2)
;(show-slice 10 11)
;(show-slice 10 11)
;(show-slice 30 50)
;(show-slice 30 90)
(show-slice 40 1208)
;(show-slice 0 20)
;(show-slice 1 2)
