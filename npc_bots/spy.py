from extra import get_opponent_source

class SpyBot():
    def __init__(self, round=1):
        self.round = round
    def move(self, previous=None):
        opponent_source = get_opponent_source(self) # is of class str
        if '3' in opponent_source:
            return 2
        return 3
