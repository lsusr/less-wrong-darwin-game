(import random)

(defclass InvertBot2 []
  (defn --init-- [self &optional [round 1]]
    (setv self.round round
          self.screenname "Invert Bot"))
  (defn move [self &optional [previous None]]
    (lif previous
         (- 5 previous)
         2)))

(defclass InvertBot3 []
  (defn --init-- [self &optional [round 1]]
    (setv self.round round
          self.screenname "Invert Bot"))
  (defn move [self &optional [previous None]]
    (lif previous
         (- 5 previous)
         3)))

(defclass RandomInvertBot23 []
  (defn --init-- [self &optional [round 1]]
    (setv self.round round
          self.screenname "Invert Bot"))
  (defn move [self &optional [previous None]]
    (lif previous
         (- 5 previous)
         (random.randint 2 3))))

(defclass InvertBot1 []
  (defn --init-- [self &optional [round 1]]
    (setv self.round round
          self.screenname "Invert Bot"))
  (defn move [self &optional [previous None]]
    (lif previous
         (- 5 previous)
         1)))

(defclass InvertBot4 []
  (defn --init-- [self &optional [round 1]]
    (setv self.round round
          self.screenname "Invert Bot"))
  (defn move [self &optional [previous None]]
    (lif previous
         (- 5 previous)
         4)))

(defclass InvertBot0 []
  (defn --init-- [self &optional [round 1]]
    (setv self.round round
          self.screenname "Invert Bot"))
  (defn move [self &optional [previous None]]
    (lif previous
         (- 5 previous)
         0)))

(defclass InvertBot5 []
  (defn --init-- [self &optional [round 1]]
    (setv self.round round
          self.screenname "Invert Bot"))
  (defn move [self &optional [previous None]]
    (lif previous
         (- 5 previous)
         5)))

(defclass RandomInvertBot []
  (defn --init-- [self &optional [round 1]]
    (setv self.round round
          self.screenname "Invert Bot"))
  (defn move [self &optional [previous None]]
    (lif previous
         (- 5 previous)
         (random.randint 0 5))))

(defclass CounterInvertBot []
  (defn --init-- [self &optional [round 1]]
    (setv self.previous None))
  (defn move [self &optional [previous None]]
    (lif previous
         (- 5 previous)
         (random.randint 2 3))))
    #_(setv output
          (lif previous
               (if (= self.previous previous)
                   previous
                   (- 5 previous))
               5))
    ;(setv self.previous output)
    ;output))
