(import random)

(defclass CementBot2 []
  (defn --init-- [self &optional [round 1]]
    (setv self.round round
          self.screenname "Invert Bot"
          self.output None))
  (defn move [self &optional [previous None]]
    (lif previous
         (do (lif-not self.output
                      (setv self.output (- 5 previous)))
             self.output)
         3)))

(defclass CementBot3 []
  (defn --init-- [self &optional [round 1]]
    (setv self.round round
          self.screenname "Invert Bot"
          self.output None))
  (defn move [self &optional [previous None]]
    (lif previous
         (do (lif-not self.output
                      (setv self.output (- 5 previous)))
             self.output)
         3)))

(defclass TwoOrThreeCement []
  (defn --init-- [self &optional [round 1]]
    (setv self.commitment (random.randint 2 3)))
  (defn move [self &optional [previous None]]
    self.commitment)
  )
